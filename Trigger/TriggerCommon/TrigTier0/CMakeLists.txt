################################################################################
# Package: TrigTier0
################################################################################

# Declare the package name:
atlas_subdir( TrigTier0 )

# Install files from the package:
atlas_install_joboptions( share/*.py )

